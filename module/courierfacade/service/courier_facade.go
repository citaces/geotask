package service

import (
	"context"
	"log"

	cservice "gitlab.com/citaces/geotask/module/courier/service"
	cfm "gitlab.com/citaces/geotask/module/courierfacade/models"
	oservice "gitlab.com/citaces/geotask/module/order/service"
)

const (
	CourierVisibilityRadius = 2500 // 2.5km
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Fatal(err)
	}
	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Fatal(err)
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Fatal(err)
	}
	orders, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")
	cs := cfm.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}
	if err != nil {
		log.Fatal(err)
	}

	return cs
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}
