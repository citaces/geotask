package service

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"gitlab.com/citaces/geotask/geo"
	modelCourier "gitlab.com/citaces/geotask/module/courier/models"
	courierService "gitlab.com/citaces/geotask/module/courier/service"
	courierMock "gitlab.com/citaces/geotask/module/courier/storage/courier_mock"
	"gitlab.com/citaces/geotask/module/courierfacade/models"
	modelOrder "gitlab.com/citaces/geotask/module/order/models"
	orderService "gitlab.com/citaces/geotask/module/order/service"
	orderMock "gitlab.com/citaces/geotask/module/order/storage/order_mock"
)

func TestCourierFacade_GetStatus(t *testing.T) {
	courier := modelCourier.Courier{
		Location: modelCourier.Point{
			Lat: courierService.DefaultCourierLat,
			Lng: courierService.DefaultCourierLng,
		},
	}
	orders := []modelOrder.Order{
		{
			ID:            1,
			Price:         1.1,
			DeliveryPrice: 2.2,
			Lng:           3.3,
			Lat:           4.4,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
		{
			ID:            2,
			Price:         2.1,
			DeliveryPrice: 3.2,
			Lng:           4.3,
			Lat:           6.4,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
	}
	cl := gomock.NewController(t)
	defer cl.Finish()

	om := orderMock.NewMockOrderStorager(cl)
	cm := courierMock.NewMockCourierStorager(cl)
	cs := courierService.NewCourierService(cm, geo.NewAllowedZone(), []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()})
	os := orderService.NewOrderService(om, geo.NewAllowedZone(), []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()})

	cm.EXPECT().GetOne(context.Background()).Return(nil, nil)
	cm.EXPECT().Save(context.Background(), courier).Return(nil)

	om.EXPECT().GetByRadius(context.Background(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Eq("m")).Return(orders, nil)

	type fields struct {
		courierService courierService.Courierer
		orderService   orderService.Orderer
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.CourierStatus
	}{
		{
			name: "GetStatus test",
			fields: fields{
				courierService: cs,
				orderService:   os,
			},
			args: args{context.Background()},
			want: models.CourierStatus{
				Courier: courier,
				Orders:  orders,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {
	courier := modelCourier.Courier{
		Location: modelCourier.Point{
			Lat: courierService.DefaultCourierLat,
			Lng: courierService.DefaultCourierLng,
		},
	}

	cl := gomock.NewController(t)
	defer cl.Finish()

	om := orderMock.NewMockOrderStorager(cl)
	cm := courierMock.NewMockCourierStorager(cl)
	cs := courierService.NewCourierService(cm, geo.NewAllowedZone(), []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()})
	os := orderService.NewOrderService(om, geo.NewAllowedZone(), []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()})

	cm.EXPECT().GetOne(context.Background()).Return(nil, nil)
	cm.EXPECT().Save(context.Background(), courier).Return(nil).Times(2)

	type fields struct {
		courierService courierService.Courierer
		orderService   orderService.Orderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "MoveCourier test",
			fields: fields{
				courierService: cs,
				orderService:   os,
			},
			args: args{
				ctx:       context.Background(),
				direction: courierService.DirectionUp,
				zoom:      60,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
