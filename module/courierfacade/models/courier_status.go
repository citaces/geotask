package models

import (
	cm "gitlab.com/citaces/geotask/module/courier/models"
	om "gitlab.com/citaces/geotask/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
