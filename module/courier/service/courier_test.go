package service

import (
	"context"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	mock_storage "gitlab.com/citaces/geotask/module/courier/storage/courier_mock"

	"gitlab.com/citaces/geotask/geo"
	"gitlab.com/citaces/geotask/module/courier/models"
	"gitlab.com/citaces/geotask/module/courier/storage"
)

func TestCourierService_GetCourier(t *testing.T) {

	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockCourierStorager(cl)

	courier := models.Courier{
		Location: models.Point{
			Lat: DefaultCourierLat,
			Lng: DefaultCourierLng,
		},
	}

	mock.EXPECT().GetOne(context.Background()).Return(nil, nil)
	mock.EXPECT().Save(context.Background(), courier).Return(nil)

	type fields struct {
		courierStorage storage.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "GetCourier test",
			fields: fields{
				courierStorage: mock,
				allowedZone:    geo.NewAllowedZone(),
				disabledZones:  []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			want:    &courier,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {

	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockCourierStorager(cl)

	courier := models.Courier{
		Location: models.Point{
			Lat: DefaultCourierLat,
			Lng: DefaultCourierLng,
		},
	}

	mock.EXPECT().Save(context.Background(), courier).Return(nil)

	type fields struct {
		courierStorage storage.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "MoveCourier test",
			fields: fields{
				courierStorage: mock,
				allowedZone:    geo.NewAllowedZone(),
				disabledZones:  []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				courier:   courier,
				direction: DirectionUp,
				zoom:      60,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
