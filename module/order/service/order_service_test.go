package service

import (
	"context"
	"reflect"
	"testing"
	"time"

	mock_storage "gitlab.com/citaces/geotask/module/order/storage/order_mock"

	"github.com/golang/mock/gomock"

	"gitlab.com/citaces/geotask/geo"
	"gitlab.com/citaces/geotask/module/order/models"
	"gitlab.com/citaces/geotask/module/order/storage"
)

func TestOrderService_GenerateOrder(t *testing.T) {
	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockOrderStorager(cl)

	mock.EXPECT().GenerateUniqueID(context.Background()).Return(int64(0), nil)
	mock.EXPECT().Save(context.Background(), gomock.Any(), orderMaxAge).Return(nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}

	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "TestOrderService_GenerateOrder",
			fields: fields{
				storage:       mock,
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{ctx: context.Background()},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_GetByRadius(t *testing.T) {

	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockOrderStorager(cl)

	orders := []models.Order{
		{
			ID:            1,
			Price:         1.1,
			DeliveryPrice: 2.2,
			Lng:           3.3,
			Lat:           4.4,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
		{
			ID:            2,
			Price:         2.1,
			DeliveryPrice: 3.2,
			Lng:           4.3,
			Lat:           6.4,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
	}

	mock.EXPECT().GetByRadius(context.Background(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Eq("m")).Return(orders, nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}

	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Order
		wantErr bool
	}{
		{
			name: "GetByRadius test",
			fields: fields{
				storage:       mock,
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				ctx:    context.Background(),
				lng:    1.1,
				lat:    2.2,
				radius: 3.3,
				unit:   "m",
			},
			want:    orders,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := o.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByRadius() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_GetCount(t *testing.T) {
	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockOrderStorager(cl)

	mock.EXPECT().GetCount(context.Background()).Return(777, nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "GetCount test",
			fields: fields{
				storage:       mock,
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			want:    777,
			wantErr: false,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := o.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCount() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_RemoveOldOrders(t *testing.T) {

	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockOrderStorager(cl)

	mock.EXPECT().RemoveOldOrders(context.Background(), orderMaxAge).Return(nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "RemoveOldOrders test",
			fields: fields{
				storage:       mock,
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			wantErr: false,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_Save(t *testing.T) {

	cl := gomock.NewController(t)
	defer cl.Finish()

	mock := mock_storage.NewMockOrderStorager(cl)

	mock.EXPECT().Save(context.Background(), gomock.Any(), orderMaxAge).Return(nil)

	type fields struct {
		storage       storage.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order models.Order
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Save test",
			fields: fields{
				storage:       mock,
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background(), models.Order{}},
			wantErr: false,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
