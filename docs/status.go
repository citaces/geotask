package docs

import "gitlab.com/citaces/geotask/module/courier/models"

// swagger:route GET /api/status courier courierStatusRequest
// Find courier with orders location.
// responses:
//   200: courierStatusResponse
//   400: description: Bad request
//	 500: description: Internal server error

// swagger:parameters courierStatusRequest
//
//nolint:all

// swagger:response courierStatusResponse
//
//nolint:all
type courierStatusResponse struct {
	// in:body
	models.Courier
}
